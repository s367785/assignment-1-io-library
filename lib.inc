section .text

; Принимает код возврата и завершает текущий процесс
exit: 
    	mov rax, 60
    	xor rdi, rdi
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	    push rdi   
	    push rsi
	    xor rax, rax
	.loop:
	    cmp byte[rax+rdi], 0
	    je .end
	    inc rax
	    jmp .loop
	.end:
	    pop rsi
	    pop rdi
	    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
    	call string_length
	pop rdi
	mov rdx, rax
	mov rsi, rdi
	mov rax, 1
	mov rdi, rax     
	syscall
	ret
 
; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	push rsi
	push rdx
	mov rax, 1
	sub rsp, 8         
	mov [rsp], rdi
	mov rdi, rax
	mov rsi, rsp
	mov rdx, rax
	syscall
	add rsp, 8
	pop rdx
	pop rsi
	pop rdi
  	  ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
   	push rdi
	mov rdi, 0xA
	call print_char
	pop rdi
    	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    	    push rdi
    	    push rdx
    	    mov rax, rdi
    	    push -1    
    	    mov rdi, 10  
	.loop_1:    				
 	    xor rdx, rdx 
    	    div rdi
   	    add rdx, '0'
    	    push rdx
   	    test al, al     
   	    jnz .loop_1
	.loop_2:
  	    pop rdx
   	    cmp rdx, -1
   	    je .end    
	    mov rdi, rdx
    	    call print_char
    	    jmp .loop_2
	.end:    
    	    pop rdx
    	    pop rdi
    	    ret
    
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    	    test rdi, rdi
    	    jns .end
    	    push rdi      
   	    mov rdi, '-' 
    	    call print_char
    	    pop rdi
    	    neg rdi
	.end:
    	    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    	    push rdi 
    	    push rsi
	.check_first:       
    	    mov al, byte [rdi]
    	    mov ah, byte [rsi]
    	    cmp al, 0
    	    jz .check_second
    	    jmp .check_equals
	.check_second:
   	    cmp ah, 0
    	    jz .end_if_equals
	.check_equals:
   	    cmp ah, 0
    	    jz .end_if_not_equals
   	    cmp al, ah
    	    jnz .end_if_not_equals
   	    inc rdi
   	    inc rsi
    	    jmp .check_first
	.end_if_equals:
   	    mov rax, 1
    	    jmp .end
	.end_if_not_equals:
   	    xor rax, rax    
	.end:
   	    pop rsi
    	    pop rdi
    	    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    	    push rdi
    	    push rsi
    	    push rdx
    	    xor rax, rax
    	    xor rdi, rdi
    	    sub rsp, 8    
    	    mov rsi, rsp
    	    mov rdx, 1
    	    syscall
    	    test eax, eax
    	    jle .end
    	    xor rax, rax
    	    lodsb 
	.end:
    	    add rsp, 8
    	    pop rdx
    	    pop rsi
    	    pop rdi
    	    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	    push rdi
	    push rsi
	    xor rdx, rdx
	.first_check:	
	    cmp rsi, 0
	    jne .read_char_loop
	    xor rax, rax
	    jmp .end_if_fail
	.read_char_loop:
	    push rdi
	    push rsi
	    call read_char
	    pop rsi
	    pop rdi
	    test al, al
	    jz .end_if_success
	    cmp al, 0x9
	    je .if_punch_sym
	    cmp al, 0xA
	    je .if_punch_sym
	    cmp al, 0x20
	    jne .if_not_punch_sym
	.if_punch_sym:	
	    test rdx, rdx
	    jz .first_check   
	    jmp .end_if_success
	.if_not_punch_sym:
	    stosb  
	    inc rdx
	    dec rsi
	    cmp rsi, -1
	    jnz .first_check
	.end_if_success:
	    xor al, al                    
	    stosb				
	    mov rax, rdi
	    pop rsi
	    pop rdi
	    mov rax, rdi
	    ret
	.end_if_fail:
	    pop rsi
	    pop rdi
    	    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    	    push rdi
	    push rsi
	    push rcx
	    push rbx
	    mov rsi, rdi
	    xor rcx, rcx
	    xor rdi, rdi
	.loop:
	    xor rax, rax
	    lodsb
	    test al, al
	    jz .end
	    sub al, '0'
	    jnb .make_a_num
	    jmp .end
	.make_a_num:
	    cmp al, 9
	    jg .end  
	    mov rbx, rax
	    mov rax, 0xA 
	    mul rdi
	    add rax, rbx
	    mov rdi, rax
	    inc rcx
	    jmp .loop
	.end:
	    mov rax, rdi
	    mov rdx, rcx
	    pop rbx
	    pop rcx
	    pop rsi
	    pop rdi
    	    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
   	    push rdi
	    push rsi
	    mov al, byte [rdi]
	    xor rsi, rsi
	    cmp al, '-'
	    jne .if_positive
	    inc rsi
	    inc rdi
	.if_positive:
	    push rsi
	    call parse_uint
	    pop rsi
	    test rsi, rsi
	    jz .end
	    neg rax
	    inc rdx
	.end:
	    pop rsi
	    pop rdi
    	    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	    push rdi
	    push rsi
	    push rdx
	    push rcx

	    push rax
	    push rdx
	    push rdi
	    push rsi
	    call string_length
	    pop rsi
	    pop rdi
	    pop rdx
	
	    cmp rax, rdx
	    jge .end
	    pop rax
	.loop:	
            xor rcx, rcx
	    mov cl, byte[rdi] 
	    mov byte[rsi], cl
	    inc rsi
	    inc rdi
	    cmp cl, 0
	    jne .loop
	    pop rcx
	    pop rdx
	    pop rsi
	    pop rdi
	    ret
	.end:    
	    pop rcx
	    pop rdx
	    pop rsi
	    pop rdi
	    pop rax
	    xor rax, rax
	    ret
